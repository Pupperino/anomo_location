#!/usr/bin/python

import cgi
import urllib2
import requests
import json
import hashlib

URL = "http://ws.anomo.com/"
VER = "v101"

login_endpoint = URL + VER + "/index.php/webservice/user/login/"
loc_endpoint = URL + VER + "/index.php/webservice/user/update_user_location/"

def main():
    form = cgi.FieldStorage()
    user = form["username"].value
    password = form["password"].value
    country = form["country"].value
    city = form["city"].value
    state = form["state"].value
    print("Content-Type: text/html\n\n")
    
    URL = "http://ws.anomo.com/"
    VER = "v101"

    login_endpoint = URL + VER + "/index.php/webservice/user/login/"
    loc_endpoint = URL + VER + "/index.php/webservice/user/update_user_location/"


    username = user
    # MD5 HASH of your password (e.g. http://www.miraclesalad.com/webtools/md5.php)
    m = hashlib.md5()
    m.update(password)
    hash = m.hexdigest()

    # Log in
    logindata = {
        "UserName": username, 
        "Password": hash
    }

    # POST
    login = requests.post(login_endpoint, logindata)
    # Get token
    token = json.loads(login.text)['token']

    # Finish up loc endpoint. Those numbers would be coordinates. Doesn't matter.
    loc_endpoint = loc_endpoint + token + '/179/90/'


    # Depends on your user settings. I go with country
    locdata = {
        "City": city,
        "Country": country,
        "State": state
    }

    # POST
    location = requests.post(loc_endpoint, data=locdata)

    # Open app, log in again. Turn of location services on your phone otherwise it'll be overwritten
    message = '''
    <html><p>Check if it worked. Open the app, log in again. Turn off location services on your phone otherwise it'll be overwritten.<br/>
    It might not work on the iOS app.
    <br />
    <br />
    Username: %s<br />
    Country: %s<br />
    State: %s<br />
    City: %s
    </html>
   '''

    print message % (user,  country, state, city)

main()
